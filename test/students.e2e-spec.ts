import { Test, TestingModule } from '@nestjs/testing';
import { StudentsModule } from '../src/students/students.module';
import { StudentsController } from '../src/students/students.controller';
import { StudentsService } from '../src/students/students.service';
import { CLIENT_TOKEN } from '../src/config/transport.config';
import { ValidationPipe, type INestApplication } from '@nestjs/common';
import { PrismaService } from '../src/prisma/prisma.service';
import { faker } from '@faker-js/faker';
import * as request from 'supertest';
import type TestAgent from 'supertest/lib/agent';
import type { Student } from '../src/students/types/student.type';
import type { Statistic } from '../src/students/types/statistic.type';
import type { GetLogResponse } from 'src/students/responses/get-log.response';

class PrismaServiceMock {
  studentsTable = faker.helpers.multiple(this.makeStudent, { count: 10 });
  statisticTable = this.studentsTable.map((student) => ({
    id: faker.number.int(),
    date: faker.date.anytime().toISOString(),
    studentId: student.personalCode,
    subject: 'biology',
    grade: faker.number.int({ min: 1, max: 5 }),
  }));

  makeStudent(): Student {
    return {
      personalCode: faker.string.uuid(),
      name: faker.person.firstName(),
      lastName: faker.person.lastName(),
    };
  }
  student = {
    findUnique: ({ where: { personalCode } }): Student =>
      this.studentsTable.find(
        (student) => student.personalCode === personalCode,
      ),
  };

  statistic = {
    findMany: ({ skip, take, orderBy }): GetLogResponse[] => {
      let slicedStatistic = this.statisticTable.slice(
        skip ?? 0,
        take ? take + skip : undefined,
      );
      if (orderBy && orderBy.date === 'asc') {
        slicedStatistic = slicedStatistic.sort(
          (a, b) => new Date(a.date).getTime() - new Date(b.date).getTime(),
        );
      }
      return slicedStatistic.map((statistic) => ({
        ...statistic,
        student: this.studentsTable.find(
          (student) => student.personalCode == statistic.studentId,
        ),
      }));
    },
  };

  getStatisticByPersonalCode(): Statistic {
    return {
      subject: 'biology',
      minGrade: 3,
      maxGrade: 5,
      avgGrade: 4,
      totalGrades: 2,
    };
  }
}

const prismaServiceMock = new PrismaServiceMock();

describe('Students e2e', () => {
  let app: INestApplication;
  let http: TestAgent;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [StudentsModule],
      controllers: [StudentsController],
      providers: [
        StudentsService,
        {
          provide: CLIENT_TOKEN,
          useValue: jest.fn(),
        },
        {
          provide: PrismaService,
          useValue: prismaServiceMock,
        },
      ],
    }).compile();

    app = module.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();

    http = request(app.getHttpServer());
  });

  afterAll(async () => {
    await app.close();
  });

  it('should return log of grades', () => {
    return http
      .get('/log')
      .expect(200)
      .expect(
        prismaServiceMock.statistic.findMany({
          skip: null,
          take: null,
          orderBy: {
            date: 'asc',
          },
        }),
      );
  });

  it('should return log of grades with pagination', () => {
    const skip = 2;
    const take = 3;
    return http
      .get(`/log?take=${take}&skip=${skip}`)
      .expect(200)
      .expect(
        prismaServiceMock.statistic.findMany({
          skip,
          take,
          orderBy: { date: 'asc' },
        }),
      );
  });

  it('should return 400 bad request when take is string', () => {
    const skip = 2;
    const take = 'string';
    return http
      .get(`/log?take=${take}&skip=${skip}`)
      .expect(400)
      .then((response) => {
        expect(
          response.body.message ==
            'take must be a number conforming to the specified constraints',
        );
      });
  });

  it('should return 400 bad request when skip is string', () => {
    const skip = 'string';
    const take = 4;
    return http
      .get(`/log?take=${take}&skip=${skip}`)
      .expect(400)
      .then((response) => {
        expect(
          response.body.message ==
            'skip must be a number conforming to the specified constraints',
        );
      });
  });

  it('should return statistic', () => {
    const { personalCode } = prismaServiceMock.studentsTable[0];
    return http
      .get(`/statistic/${personalCode}`)
      .expect(200)
      .expect({
        student: prismaServiceMock.student.findUnique({
          where: { personalCode },
        }),
        statistic: prismaServiceMock.getStatisticByPersonalCode(),
      });
  });
});
