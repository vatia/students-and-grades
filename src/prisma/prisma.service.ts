import { Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaClient, type PrismaPromise } from '@prisma/client';
import type { Statistic } from '../students/types/statistic.type';

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit {
  async onModuleInit() {
    await this.$connect();
  }

  getStatisticByPersonalCode(personalCode: string): PrismaPromise<Statistic[]> {
    return this.$queryRaw`SELECT subject, max(grade) as "maxGrade",
                 min(grade) AS "minGrade", avg(grade) AS "avgGrade",
                 count(grade)::int AS "totalGrades"
                 FROM "Statistic" 
                 WHERE "studentId" = ${personalCode}
                 GROUP BY subject`;
  }
}
