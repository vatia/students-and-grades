import { IsInt, IsString } from 'class-validator';

class EventGradeData {
  @IsString()
  personalCode: string;

  @IsInt()
  grade: number;

  @IsString()
  subject: string;
}

export class EventGradeDto {
  data: EventGradeData;
}
