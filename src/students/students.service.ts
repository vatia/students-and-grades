import { Inject, Injectable } from '@nestjs/common';
import { CLIENT_TOKEN } from '../config/transport.config';
import { PrismaService } from '../prisma/prisma.service';
import { StudentsTopic } from './types/topics.enum';
import type { ClientProxy } from '@nestjs/microservices';
import type { GetLogOfGradedDto } from './dto/get-log-of-graded.dto';
import type { EventGradeDto } from './dto/event-grade.dto';
import type { PrismaPromise } from '@prisma/client';
import type { Student } from './types/student.type';
import type { Grade } from './types/grade.type';
import type { GetLogResponse } from './responses/get-log.response';
import type { GetStatisticResponse } from './responses/get-statistic.response';

@Injectable()
export class StudentsService {
  constructor(
    @Inject(CLIENT_TOKEN)
    private readonly client: ClientProxy,
    private readonly prisma: PrismaService,
  ) {}

  getLog({ take, skip }: GetLogOfGradedDto): PrismaPromise<GetLogResponse[]> {
    return this.prisma.statistic.findMany({
      select: {
        date: true,
        subject: true,
        grade: true,
        student: {
          select: {
            personalCode: true,
            name: true,
            lastName: true,
          },
        },
      },
      skip,
      take,
      orderBy: {
        date: 'asc',
      },
    });
  }

  async getStatistic(personalCode: string): Promise<GetStatisticResponse> {
    const student = await this.getStudent(personalCode);
    const statistic =
      await this.prisma.getStatisticByPersonalCode(personalCode);
    return {
      student,
      statistic,
    };
  }

  private async getStudent(personalCode: string): Promise<Student> {
    let student: Student;
    try {
      student = await this.findStudentByPersonalCode(personalCode);
      if (!student) {
        student = await this.resolveStudent(personalCode);
      }
      return student;
    } catch (e) {
      throw new Error(e.code);
    }
  }
  private resolveStudent(personalCode: string): Promise<Student> {
    return new Promise((resolve, reject) => {
      const studentObservable = this.client.send(StudentsTopic.GET, {
        personalCode,
      });
      studentObservable.subscribe((student) => {
        if (student.data) {
          resolve(student.data);
        }
        if (student.error) {
          reject(student.error);
        }
        reject(`Unexpected response from students-microservice`);
      });
    });
  }

  private findStudentByPersonalCode(
    personalCode: string,
  ): PrismaPromise<Student> {
    return this.prisma.student.findUnique({
      where: {
        personalCode,
      },
    });
  }

  async saveEventGrade({
    data: { personalCode, grade, subject },
  }: EventGradeDto): Promise<void> {
    const student = await this.getStudent(personalCode);
    await this.saveGradeAndSubjAndStud(
      { personalCode, grade, subject },
      student,
    );
  }

  private async saveGradeAndSubjAndStud(
    { personalCode, grade, subject }: Grade,
    { name, lastName }: Student,
  ): Promise<void> {
    await this.prisma.statistic.create({
      data: {
        grade,
        date: new Date(),
        subject,
        student: {
          connectOrCreate: {
            where: {
              personalCode,
            },
            create: {
              personalCode,
              name,
              lastName,
            },
          },
        },
      },
    });
  }
}
