import { Module } from '@nestjs/common';
import { StudentsService } from './students.service';
import { StudentsController } from './students.controller';
import { ClientsModule } from '@nestjs/microservices';
import transportConfig, { CLIENT_TOKEN } from '../config/transport.config';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PrismaService } from '../prisma/prisma.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [transportConfig],
    }),
    ClientsModule.registerAsync([
      {
        name: CLIENT_TOKEN,
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (config: ConfigService) => {
          const transport = config.get('nats.transport');
          const url = config.get('nats.url');
          return {
            transport,
            options: {
              url,
            },
          };
        },
      },
    ]),
  ],
  controllers: [StudentsController],
  providers: [StudentsService, PrismaService],
})
export class StudentsModule {}
