import { Controller, Get, Param, Query } from '@nestjs/common';
import { StudentsService } from './students.service';
import { StudentsTopic } from './types/topics.enum';
import { EventPattern, Payload } from '@nestjs/microservices';
import type { GetLogOfGradedDto } from './dto/get-log-of-graded.dto';
import type { GetLogResponse } from './responses/get-log.response';
import type { GetStatisticResponse } from './responses/get-statistic.response';
import type { EventGradeDto } from './dto/event-grade.dto';

@Controller()
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @Get('/log')
  getLog(@Query() pagination: GetLogOfGradedDto): Promise<GetLogResponse[]> {
    return this.studentsService.getLog(pagination);
  }

  @Get('/statistic/:personalCode')
  getStatisticByPersonalCode(
    @Param('personalCode') personalCode: string,
  ): Promise<GetStatisticResponse> {
    return this.studentsService.getStatistic(personalCode);
  }

  @EventPattern(StudentsTopic.SUBSCRIBE)
  subscribeStudents(@Payload() eventGradeDto: EventGradeDto): Promise<void> {
    return this.studentsService.saveEventGrade(eventGradeDto);
  }
}
