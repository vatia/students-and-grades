import { Student } from '../types/student.type';

export interface GetLogResponse {
  date: Date | string;
  subject: string;
  grade: number;
  student: Student;
}
