import { Statistic } from '../types/statistic.type';
import { Student } from '../types/student.type';

export interface GetStatisticResponse {
  student: Student;
  statistic: Statistic[];
}
