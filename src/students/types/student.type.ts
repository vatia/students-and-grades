export type Student = {
  personalCode: string;
  name: string;
  lastName: string;
};
