export type Grade = {
  personalCode: string;
  grade: number;
  subject: string;
};
