export enum StudentsTopic {
  GET = 'students.v1.get',
  SUBSCRIBE = 'students.v1.graded',
}
