export type Statistic = {
  subject: string;
  maxGrade: number;
  minGrade: number;
  avgGrade: number;
  totalGrades: number;
};
