import { registerAs } from '@nestjs/config';
import { Transport } from '@nestjs/microservices';

export default registerAs('nats', () => ({
  transport:
    Transport[process.env.MICROSERVICE_TRANSPORT_TYPE] ??
    DEFAULT_TRANSPORT_TYPE,
  url: process.env.TRANSPORT_URL || DEFAULT_TRANSPORT_URL,
}));

export const CLIENT_TOKEN = 'PUBSUB';
export const DEFAULT_TRANSPORT_TYPE = Transport.NATS;
export const DEFAULT_TRANSPORT_URL = 'nats://localhost:4222';
