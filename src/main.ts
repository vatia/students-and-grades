import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { MicroserviceOptions } from '@nestjs/microservices';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  const config = app.get(ConfigService);
  const transport = config.get('nats.transport');
  app.connectMicroservice<MicroserviceOptions>({
    transport,
    options: {
      url: config.get('nats.url'),
    },
  });
  app.startAllMicroservices();

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Grades')
    .setDescription('The grades API description')
    .setVersion('1.0')
    .addTag('grades')
    .build();

  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('api', app, document);

  await app.listen(3002);
}
bootstrap();
