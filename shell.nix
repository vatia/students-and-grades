{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    nativeBuildInputs = with pkgs.buildPackages; [
      nodejs_21
      prisma-engines
      openssl
      docker-compose
      pgcli
    ];

    PRISMA_QUERY_ENGINE_LIBRARY="${pkgs.prisma-engines.outPath}/lib/libquery_engine.node";
    PRISMA_SCHEMA_ENGINE_BINARY="${pkgs.prisma-engines.outPath}/bin/schema-engine";
    PRISMA_QUERY_ENGINE_BINARY="${pkgs.prisma-engines.outPath}/bin/query-engine";
    DB_PORT=5632;
    DB_USER="statistic";
    DB_PASSWORD=123;
    DATABASE_URL="postgresql://statistic:123@localhost:5632/statistic?schema=public";

    shellHook = ''
      npm run docker:dev
    '';
}
